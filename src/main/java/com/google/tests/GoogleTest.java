package com.google.tests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class GoogleTest {
	WebDriver driver;

	/**
	 * @throws InterruptedException
	 * 
	 *             Navigate to google browser and
	 */
	@Test
	public void googleTest() throws InterruptedException {
		driver.get("http://google.com");
		System.out.println("Navigate to 'http://google.com'");

		driver.findElement(By.id("lst-ib")).sendKeys("Selenium");
		System.out.println("Input search text: 'Selenium'");
		driver.findElement(By.name("btnG")).click();
		System.out.println("Click on icon 'Search'");

		Thread.sleep(10000);

		System.out.println("Added new log test 2!");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\Binary\\chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@AfterMethod
	public void afterMethod() {
		if (driver != null) {
			driver.close();
			driver.quit();
		}
	}

}
